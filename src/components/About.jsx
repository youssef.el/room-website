import React from 'react'
import image1 from '../assets/images/image-about-dark.jpg'
import image2 from '../assets/images/image-about-light.jpg'

const About = () => {
  return (
    <div className='mx-auto flex flex-col lg:flex-row'>
      <img src={image1} alt="" />
      <div className='flex flex-col p-6 py-10 space-y-4 '>
        <h2 className='font-semibold text-md uppercase tracking-[8px]'>About our furniture</h2>
        <p className='text-gray-400'>Our multifunctional collection blends design and function to suit
          your individual taste. Make each room unique, or pick a cohesive
          theme that best express your interests and what inspires you. Find
          the furniture pieces you need, from traditional to contemporary
          styles or anything in between. Product specialists are available 
          to help you create your dream space.
        </p>
      </div>
      <img src={image2} alt="" />
    </div>
  )
}

export default About
