import React from 'react'
import Header from '../components/Header'
import {CgArrowLongRight} from 'react-icons/cg'
import {SlArrowRight ,SlArrowLeft } from 'react-icons/sl'

const HeroElement = (props) => {
  return (
    <div className=' flex flex-col lg:grid lg:grid-cols-3 lg:space-y-[100px]  '>
        {/* left side */}
      <div className='col-span-2 relative ' >
        <Header />
        <img src={props.image} className='w-full  h-[500px] lg:h-full '  />
      </div>
        {/* right side */}
      <div className='flex flex-col-reverse lg:flex-col space-y-[-64px] lg:space-y-10 mx-auto '>
        <div className='flex flex-col space-y-4 p-4 lg:p-10'>
            <h1 className='text-4xl font-bold'>{props.title}</h1>
            <p className='text-gray-400'>{props.text}</p> 
            <div className='flex items-center space-x-4'>
                <button className='btn btn-link p-0 text-black no-underline tracking-[10px]	 hover:no-underline hover:text-gray-400'>Shop Now </button>
                <CgArrowLongRight size={25} />
            </div>
        </div>
        <div className='flex place-self-end lg:place-self-start'>
            <div>
            <SlArrowLeft size={20} onClick={props.next}  className='translate-x-0  w-[70px] h-[64px] p-5 bg-black text-white ' />  
            </div> 
            <div>
            <SlArrowRight size={20} onClick={props.prev} className='translate-x-0  w-[70px] h-[64px] p-5 bg-black text-white ' />
            </div>
        </div>
        
      </div>

    </div>
  )
}

export default HeroElement
